"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VALID_PARCEL = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const decentraland_crypto_middleware_1 = require("decentraland-crypto-middleware");
const axios_1 = __importDefault(require("axios"));
const securityChecks_1 = require("./security/securityChecks");
const utils_1 = require("./utils");
exports.VALID_PARCEL = [1, 1];
const port = 8080; // default port to listen
const port2 = 8081; // default port to listen
const app = (0, express_1.default)();
const app2 = (0, express_1.default)();
app.use((0, cors_1.default)({ origin: true }));
app.use(express_1.default.json());
app2.use((0, cors_1.default)({ origin: true }));
app2.use(express_1.default.json());
app.post("/check-validity", (0, decentraland_crypto_middleware_1.express)({ expiration: utils_1.VALID_SIGNATURE_TOLERANCE_INTERVAL_MS }), (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield (0, securityChecks_1.runChecks)(req
        // VALID_PARCEL
        );
        Object.assign(req.body, {
            auth: req.auth,
            authMetadata: req.authMetadata,
        });
        res.status(200).send({ valid: true, msg: "Valid request" });
        axios_1.default.post("http://localhost:8081/", req.body);
    }
    catch (error) {
        console.log(error.message);
        return res
            .status(400)
            .send({ valid: false, error: `Can't validate your request` });
    }
}));
app2.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        console.log(req === null || req === void 0 ? void 0 : req.body);
        return res.status(200).send({ valid: true, msg: "Valid request" });
    }
    catch (error) {
        console.log(error.message);
        return res
            .status(400)
            .send({ valid: false, error: `Can't validate your request` });
    }
}));
// start the Express server
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
app2.listen(port2, () => {
    console.log(`server started at http://localhost:${port2}`);
});
//# sourceMappingURL=index.js.map