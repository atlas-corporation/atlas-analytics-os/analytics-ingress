# analytics-ingress

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* **YOU ARE HERE** `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

* `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

* `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

*  `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About analytics-ingress

This app is a node.js express application that receives data on a single endpoint and loads it into the database. This application should be run independent of other applications as it is mission critical - this being down will lead to failures in data collection which cannot be fixed later. All other applications can be deployed at a later date and used to enhance and report on analytics data but this application cannot go down or data will be lost forever.

This data also enhances the data on the way in by
* splitting the data into a genesis-city collection and a "everything else" collection which is used for worlds.
* enhancing the data by converting a genesis city position to a parcel x,y
* If enabled, performing source checks such as signed fetch or origin checks

## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t analytics-ingress . && docker run analytics-ingress`. It can be run in conjunction with hosting services in order to expose the selected domain via https to reduce errors when receiving data from within world. 