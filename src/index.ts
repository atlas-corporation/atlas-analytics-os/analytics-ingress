import express, { Request, Response } from "express";
import cors from "cors";
import dcl, { express as dclExpress } from "decentraland-crypto-middleware";
import axios from "axios";

import { runChecks } from "./security/securityChecks";
import { VALID_SIGNATURE_TOLERANCE_INTERVAL_MS, Metadata } from "./utils";

import { MongoClient } from 'mongodb';
export const VALID_PARCEL: number[] = [1, 1];

const port = 8080; // default port to listen
const app = express();

// Database Name
import dotenv from 'dotenv';
dotenv.config();

const dbName = process.env.db_env;
const mongo_pw = process.env.mongo_pw
const mongo_user = process.env.mongo_user
const mongo_host = process.env.mongo_host

// Create a new MongoClient
const client = new MongoClient("mongodb+srv://"+mongo_user+":"+mongo_pw+"@"+mongo_host);

app.use(cors());
app.use(express.json());

app.post(
  "/",
  dclExpress({ expiration: VALID_SIGNATURE_TOLERANCE_INTERVAL_MS }),
  async (
    req: Request & dcl.DecentralandSignatureData<Metadata>,
    res: Response
  ) => {
    try {
      // UNCOMMENT THIS IF YOU CARE ABOUT THE ORIGIN OF THIS REQUEST AND WANT TO TAKE THIS SERIOUSLY
      //await runChecks(
      //  req
        //VALID_PARCEL
      //);
      Object.assign(req.body, {
        auth: req.auth,
        userAgent: req.header('User-Agent'),
        ip: req.header('X-Forwarded-For'),
        sceneId: req.authMetadata.sceneId
      });
      res.status(200).send({ valid: true, msg: "Valid request" });
      // Compute parcel
      try {
        req.body.parcel = [
          Math.floor(req.body.playerPosition.x / 16),
          Math.floor(req.body.playerPosition.z / 16)
        ];
      } catch (error) {
        console.log("Error occurred while calculating parcel:", error.message);
        req.body.parcel = [0,0]; // Define an empty parcel in case of an error
      }

      if([	"artemis",
            "athena",
            "baldr",
            "dg",
            "loki",
            "heimdallr",
            "hela",
            "hephaestus",
            "hera",
            "marvel",
            "odin",
            "unicorn",
            "main"].indexOf(req.body.realm) > -1){
    

        // INSERT INTO DB HERE      
        const db = client.db(dbName);
        const collection = db.collection('analytics');

        const document = req.body;
        collection.insertOne(document)
          .catch(err => console.error(`Failed to insert item: ${err}`));
      
      }else{

        // INSERT INTO DB HERE - M        
        const db = client.db(dbName);
        const collection = db.collection('analytics_private');

        // Data to insert, e.g., the modified request body
        const document = req.body;
        collection.insertOne(document)
          .catch(err => console.error(`Failed to insert item: ${err}`));
      }
    } catch (error) {
      console.log("signed-fetch failure",error.message);
      return res
        .status(400)
        .send({ valid: false, error: `Can't validate your request` });
    }
  }
);

app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
