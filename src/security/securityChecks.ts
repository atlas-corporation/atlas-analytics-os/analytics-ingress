import { Request } from "express";
import dcl from "decentraland-crypto-middleware";

import { denyListedIPS, TESTS_ENABLED, Metadata } from "../utils";
import { checkCoords, checkPlayer } from "./verifyOnMap";

export function checkOrigin(req: Request) {
  const validOrigins = [
    "https://play.decentraland.org",
    "https://play.decentraland.zone",
  ];
  return validOrigins.includes(req.header("origin"));
}

export function checkBannedIPs(req: Request) {
  const ip = req.header("X-Forwarded-For");
  return denyListedIPS.includes(ip);
}

export async function runChecks(
  req: Request & dcl.DecentralandSignatureData<Metadata>,
  parcel?: number[]
) {
  const metadata = req.authMetadata;
  const userAddress = req.auth;
  const eventName = req.body.eventName;
  const coordinates = metadata.parcel.split(",").map((item: string) => {
    return parseInt(item, 10);
  });

  let realm = ""
  if(!metadata.realm.serverName){
    realm = metadata.realm.serverName;
  }else if(!metadata.realm.domain){
    realm = metadata.realm.hostname;
  }else{
    realm = metadata.realm.domain;
  }

  console.log("realm: " + String(realm))
  let catalyst = ""
  if(!metadata.realm.hostname){
    catalyst = metadata.realm.catalystName;
  }else{
    catalyst = metadata.realm.hostname
  }
  console.log("catalyst: " + String(catalyst))
  // check that the request comes from a decentraland domain
  const validOrigin =
    TESTS_ENABLED && (catalyst === "127.0.0.1:8000" || catalyst === "http://127.0.0.1:8000")
      ? true
      : checkOrigin(req);
  if (!validOrigin) {
    throw new Error("INVALID ORIGIN");
  }

  // filter against a denylist of malicious ips
  const validIP = checkBannedIPs(req);
  if (validIP) {
    throw new Error("INVALID IP");
  }


  // Validate that the authchain signature is real
  // validate that the player is in the catalyst & location from the signature
  const validCatalystPos: boolean =
  TESTS_ENABLED && (catalyst === "127.0.0.1:8000" || catalyst === "http://127.0.0.1:8000")
      ? true
      : await checkPlayer(userAddress, catalyst, coordinates);
  if (!validCatalystPos) {
    if(eventName !== "load-timer"){
      throw new Error("INVALID PLAYER POSITION");
    }
  }

  // validate that the player is in a valid location for this operation - if a parcel is provided
  const validPos: boolean = parcel?.length
    ? checkCoords(coordinates, parcel)
    : true;

  if (!validPos) {
    throw new Error("INVALID PARCEL POSITION");
  }
}
